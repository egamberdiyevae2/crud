// ! BU yerda barcha bizga kerakli elementlarni selectorlar orqali olibmiz.

// todo)) Quyidagi todo local storage da saqlangan ma'lumotlarni automatic tarzda saqlab qoladi va biz website ni refresh
// todo)) qilsak ham oldin kiritilgan ma'lumotlar saqlanib qoladi.

const formCreate = document.getElementById("form-create");
const formEdit = document.getElementById("form-edit");
const listGroupTodo = document.getElementById("list-group-todo");
// const messageCreate = document.getElementById('message-create')
const time = document.getElementById("time");
const modal = document.getElementById("modal");
const overlay = document.getElementById("overlay");
/* time elements */
const fullDay = document.getElementById("full-day");
const hourEl = document.getElementById("hour");
const minuteEl = document.getElementById("minute");
const secondEl = document.getElementById("second");
const closeEl = document.getElementById("close");

let editItemId;

// check
// ! BU YERDA LOCAL STORAGEGA BIZ OLDIN KIRITIB SAQLAGAN MA'LUMOTLAR SAQLANADI, agar oldin save qmagan bo'lsak
// ! bo'sh array chiqadi.
let todos = JSON.parse(localStorage.getItem("list")) //! bu yerdagi qiymat null qaytaradi agar oldin hech narsa kiritmagan bo'lsak.
  ? JSON.parse(localStorage.getItem("list"))
  : []; //!tepadagi ternaryda null chiqsa elsega tushib bo'sh arrayni qaytaradi.

if (todos.length) showTodos(); //! Pastdegi showTodos() function chaqirildi;

// setTodos to localstorage //! local storage ga elementni qo`shadi
function setTodos() {
  //! //checkda // nimadir o`zgarsa local storagega save bo`ladi.
  localStorage.setItem("list", JSON.stringify(todos)); //! list key orqali local storagega string ko'rinishda saqlaymiz.
}

// time

function getTime() {
  const now = new Date();
  const date = now.getDate() < 10 ? "0" + now.getDate() : now.getDate();
  const month =
    now.getMonth() < 10 ? "0" + (now.getMonth() + 1) : now.getMonth(); //! oylar har doim bitta kam yurgani uchun +1 qo'yiladi
  const year = now.getFullYear();

  const hour = now.getHours() < 10 ? "0" + now.getHours() : now.getHours();
  const minute =
    now.getMinutes() < 10 ? "0" + now.getMinutes() : now.getMinutes();
  const second =
    now.getSeconds() < 10 ? "0" + now.getSeconds() : now.getSeconds();

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  const month_title = now.getMonth();
  fullDay.textContent = `${date} ${months[month_title]}, ${year} `;

  hourEl.textContent = hour;
  minuteEl.textContent = minute;
  secondEl.textContent = second;

  return `${hour}:${minute}, ${date}.${month}.${year}`;
}

setInterval(getTime, 1000); //!get time functionni qayta qayta chaqirish uchun ishlatiladi

// show todos

function showTodos() {
  const todos = JSON.parse(localStorage.getItem("list")); //! agar local storage da list nomli key bolsa o'shani itemlarini o`zida array ko`rinishida saqlaydi
  listGroupTodo.innerHTML = ""; //! Bu yordamida biz foeachda element har aylanganda oldingi qiymatlarni qo'shvormasligi uchun "" ga tengladik;
  todos.forEach((item, i) => {
    //  ? pastda ulni ushlab olib uning li lariga qiymat bermoqchimiz.
    listGroupTodo.innerHTML +=
      //! bu yerda har bir qo'shmoqchi bo'lgan listimizga quyidagicha list yasalib boradi.
      `
        <li  onclick="setCompleted(${i})" class="list-group-item d-flex justify-content-between ${
        item.completed == true ? "complated" : ""
      } ">
             ${item.text}
        <div class="todo-icons">
          <span class="opacity-50 me-2">${item.time}</span>
          <i class="fas fa-pen me-2 text-warning" onclick=(editTodo(${i})) src="./img/edit.svg" alt="edit icon" width="25" height="25"></i>
          <i class="fas fa-trash text-danger" onclick=(deleteTodo(${i}))  src="./img/delete.svg" alt="delete icon" width="25" height="25"></i>
        </div>
      </li>
    `;
  });
}

// show error
// ! BU yerda agar biz add button ni inputga qiymat bermasdan bossak pasda qizilda xatoni chiqazib beradigan function yaratildi
// ! va qayerga message joylashimiz beriladi. Bu modalgayam ta'sir qiladi.
function showMessage(where, message) {
  document.getElementById(`${where}`).textContent = message;

  setTimeout(() => {
    document.getElementById(`${where}`).textContent = ""; //? bu yerda pastda berilgan vaqtda(2.5 second) tepadagi messageni "" ga tenglab qo'yadi
  }, 2500);
}

/*<
 */
// get Todos
// ! todoni ovolamiz
formCreate.addEventListener("submit", (e) => {
  //! HTMLdagi formani ichidan inputni ovolishimiz uchun formani saqlagan variableni ishlatyapmiz!
  // ! va submit eventni qo'shamiz submit bo'lganda function ishga tushadi
  e.preventDefault(); //? Agar biz add button ni bossak sahifa yangilanmasligi uchun qo'shamiz
  const todoText = formCreate["input-create"].value.trim(); //? Inputni valuesini olamiz
  //! bu yerdagi ["id"] -- ikkita so'z va - dan foydalanganligi uchun qo'shildi.
  // ! trim() - 1tadan ko'p 'space' bo'lsa, ularni bittaga aylantiradi(boshdagi bln oxiridagi o'chib ketadi) va faqat spacelani yozsak error chiqadi
  formCreate.reset(); //? Bu biz inputga yozib add qganimizdan keyin eski qiymatni o`chirvoradi
  if (todoText.length) {
    // agar todoText.length bor bo'lsa ishlaydi
    //? Bu ifni inputga hech narsa kiritilmasligini oldini olishda ishlatyapmiz
    todos.push({ text: todoText, time: getTime(), completed: false });
    // ? bu bizga local storagega malumotlarimizni saqlashimiz un kk. bunda objectdan foydalanamiz, unda asosan 3 ta narsa bo'lad
    // ? 1. text- biz qo'shmoqchi bolgan text, 2.time- qaysi paytda qo`shilgani(soat va vaqtni o`z ichiga olgan function yaratdik getTime()da),
    // ? 3. completed- bajarilgan yoki yo`qligi.
    setTodos();
    showTodos(); //! tepada yasalgan showtodos() function chaqirilyapti
  } else {
    showMessage("message-create", "Please, Enter some todo..."); //! Show message function chaqirildi va kerakli parametrlar berildi
    // ! Ya'ni "where" -- qayerga message yozish kerakligi.
    // ? [message-create-- id] == 'where' va "Please, Enter some todo..." == 'message' bu chiqadiga message.
  }
});

// delete todo
function deleteTodo(index) {
  // ! itemni o'chirish
  const deletedTodos = todos.filter((item, i) => {
    //? todos ni filter qiladi bunda item keladi, uni indexiyam boladi
    return i !== index; //?agar bu shart to'g'ri bo'lsa deletedTodos ga elementlani push qiladi
  });

  todos = deletedTodos; // todos deletedTodos ga o'zgartirdi
  setTodos();
  showTodos(); // qaytadan yasaydi.
}

// setCompleted
function setCompleted(index) {
  // ! bu yerda ekranni bosganda yozuv hiralashadi
  const completedTodos = todos.map((item, i) => {
    if (index == i) {
      return { ...item, completed: item.completed == true ? false : true };
    } else {
      return { ...item };
    }
  });
  todos = completedTodos;
  setTodos();
  showTodos();
}

// edit Form
formEdit.addEventListener("submit", (e) => {
  //! bu joyda kiritilgan edited itemni eski itemning o'rniga qo'yish uchun logika yoziladi
  e.preventDefault();

  const todoText = formEdit["input-edit"].value.trim();
  formEdit.reset();
  if (todoText.length) {
    todos.splice(editItemId, 1, {
      text: todoText,
      time: getTime(),
      completed: false,
    });
    setTodos();
    showTodos();
    close();
  } else {
    showMessage("message-edit", "Please, Enter some todo...");
  }
});

// editTodo
function editTodo(index) {
  //!bu bosganda modal chiqishi va qaysi elementni o'zgartirmoqchi bo'lsa oshani indexi editItemId ga saqlanadi
  open();
  editItemId = index;
}

overlay.addEventListener("click", close); //! overlay bosganda close function chaqiriladi
closeEl.addEventListener("click", close); //! X ni bosganda close function chaqiriladi

document.addEventListener("keydown", (e) => {
  //! esc keyboard uchun -> bosganda modaldan chiqadi
  if (e.which == 27) {
    close();
  }
});

function open() {
  //! modal ochilganda yopish uchun function
  modal.classList.remove("hidden");
  overlay.classList.remove("hidden");
}

function close() {
  //! modal yopilganda ochish uchun function
  modal.classList.add("hidden");
  overlay.classList.add("hidden");
}
